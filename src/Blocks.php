<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;

/**
 * Scripts
 */
class Blocks
{
	/**
	 * Actualizar las versiones
	 */
	public static function update(): array
	{
		$answer = [];
		$dir    = getcwd();
		MerakEnv::isMerak();

		/***** BLOCK.JS *****/

		// Buscar los scripts en el directorio de bloques
		$files = glob('assets/blocks/*/block.js');
		$text  = '';
		foreach ($files as $file) {
			$text .= str_replace(['assets/blocks/', '.js'], ['./', ''], sprintf("import '%s';\n", $file));
		}

		// Guardar archivo o eliminar
		$filename = $dir . '/assets/blocks/block.js';
		if ($text != '') {
			file_put_contents($filename, $text);
			$answer[] = 'block.js';
		} elseif (file_exists($filename)) {
			unlink($filename);
		}

		/***** FRONTEND.JS *****/

		// Buscar los scripts en el directorio de bloques
		$files = glob('assets/blocks/*/frontend.js');
		$text  = '';
		foreach ($files as $file) {
			$text .= str_replace(['assets/blocks/', '.js'], ['./', ''], sprintf("import '%s';\n", $file));
		}

		// Guardar archivo o eliminar
		$filename = $dir . '/assets/blocks/frontend.js';
		if ($text != '') {
			file_put_contents($filename, $text);
			$answer[] = 'frontend.js';
		} elseif (file_exists($filename)) {
			unlink($filename);
		}

		/***** DASHBOARD.JS *****/

		// Buscar los scripts en el directorio de bloques
		$files = glob('assets/blocks/*/dashboard.js');
		$text  = '';
		foreach ($files as $file) {
			$text .= str_replace(['assets/blocks/', '.js'], ['./', ''], sprintf("import '%s';\n", $file));
		}

		// Guardar archivo o eliminar
		$filename = $dir . '/assets/blocks/dashboard.js';
		if ($text != '') {
			file_put_contents($filename, $text);
			$answer[] = 'dashboard.js';
		} elseif (file_exists($filename)) {
			unlink($filename);
		}

		/***** EDITOR.SCSS *****/

		// Buscar los scripts en el directorio de bloques
		$files = glob('assets/blocks/*/editor.{css,scss}', GLOB_BRACE);
		$text  = '';
		foreach ($files as $file) {
			$exp = explode('/', $file);
			$text .= str_replace(['assets/blocks/'], ['./'], sprintf("@use '%s' as %s;\n", $file, $exp[2]));
		}

		// Guardar archivo o eliminar
		$filename = $dir . '/assets/blocks/editor.scss';
		if ($text != '') {
			file_put_contents($filename, $text);
			$answer[] = 'editor.scss';
		} elseif (file_exists($filename)) {
			unlink($filename);
		}

		/***** STYLE.SCSS *****/

		// Buscar los scripts en el directorio de bloques
		$files = glob('assets/blocks/*/style.{css,scss}', GLOB_BRACE);
		$text  = '';
		foreach ($files as $file) {
			$exp = explode('/', $file);
			$text .= str_replace(['assets/blocks/'], ['./'], sprintf("@use '%s' as %s;\n", $file, $exp[2]));
		}

		// Guardar archivo o eliminar
		$filename = $dir . '/assets/blocks/style.scss';
		if ($text != '') {
			file_put_contents($filename, $text);
			$answer[] = 'style.scss';
		} elseif (file_exists($filename)) {
			unlink($filename);
		}

		return $answer;
	}
}
