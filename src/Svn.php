<?php

namespace MerakEnv;

use MerakEnv\MerakEnvException;

/**
 * Scripts
 */
class Svn
{
	public static function sync(): bool
	{
		if (!file_exists(getcwd() . DIRECTORY_SEPARATOR . 'svn-config.php')) {
			throw new MerakEnvException('No svn-config.php file.', MerakEnvException::NO_SVN_CONFIG);
		}

		$config = require(getcwd() . DIRECTORY_SEPARATOR . 'svn-config.php');

		if (!file_exists($config['SVN_DIRECTORY'] . DIRECTORY_SEPARATOR . 'trunk')) {
			throw new MerakEnvException('No svn directory.', MerakEnvException::NO_SVN_DIRECTORY);
		}

		if (!file_exists(getcwd() . DIRECTORY_SEPARATOR . $config['ZIP_FILE'])) {
			throw new MerakEnvException('No install file.', MerakEnvException::NO_ZIP);
		}

		$version   = false;
		$file_data = file_get_contents(getcwd() . DIRECTORY_SEPARATOR . $config['PLUGIN_NAME'] . '.php');
		// Buscar el elemento que tiene la versión
		if (preg_match_all('/Version: ([0-9]+\.[0-9]+\.[0-9]+)/i', $file_data, $result)) {
			if (isset($result[1][0])) {
				$version = $result[1][0];
			}
		}

		if (!$version) {
			throw new MerakEnvException("Can't determine version number.", MerakEnvException::NO_VERSION);
		}

		self::rmdir_recursive($config['SVN_DIRECTORY'] . DIRECTORY_SEPARATOR . 'trunk');

		$unzip = sprintf('unzip -uo %s -d %s', getcwd() . DIRECTORY_SEPARATOR . $config['ZIP_FILE'], $config['SVN_DIRECTORY'] . DIRECTORY_SEPARATOR . 'trunk');
		exec($unzip);

		$current_dir = getcwd();
		chdir($config['SVN_DIRECTORY']);

		$svn_commit = sprintf("svn ci -m 'v%s'", $version);
		system($svn_commit);
		system("svn up");

		$svn_cp = sprintf("svn cp trunk 'tags/%s'", $version);
		system($svn_cp);
		$svn_commit = sprintf("svn ci -m 'tagging version %s'", $version);
		system($svn_commit);

		chdir($current_dir);

		return true;
	}

	private static function rmdir_recursive($directory, $delete_parent = false)
	{
		$files = glob($directory . '/{,.}[!.,!..]*', GLOB_MARK | GLOB_BRACE);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::rmdir_recursive($file, 1);
			} else {
				unlink($file);
			}
		}
		if ($delete_parent) {
			rmdir($directory);
		}
	}
}
