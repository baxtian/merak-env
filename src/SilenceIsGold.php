<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;

/**
 * Scripts
 */
class SilenceIsGold
{
	/**
	 * Revisa si hay problemas
	 *
	 * @return boolean
	 */
	static public function apply() : bool
	{
		$answer = false;
		$dir    = getcwd();
		MerakEnv::isMerak();

		$directorios = [
			'./build/',
			'./languages/',
			'./src/',
			'./templates/',
		];

		$files = [];
		foreach ($directorios as $directorio) {
			$path = realpath($directorio);
			if ($path !== false and is_dir($path)) {
				$it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directorio));
				$it->rewind();
				foreach ($it as $file) {
					if ($file->isDir()) {
						$path = $file->getPathname();
						if (substr($path, -2) == '/.') {
							// Estamos en un directorio
							// Revisar si existe el archivo
							$filename = $path . '/index.php';
							if (!realpath($filename)) {
								// Si no existe crear el archivo de silencio
								file_put_contents($filename, '<?php' . PHP_EOL . '//Silence is gold');
							}
						}
					}
				}
			}
		}

		// Archivo en raiz del proyecto
		$filename = './index.php';
		if (!realpath($filename)) {
			// Si no existe crear el archivo de silencio
			file_put_contents($filename, '<?php' . PHP_EOL . '//Silence is gold');
		}

		return true;
	}
}
