<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;
use SplHeap;
use SplFileInfo;
use Iterator;

class SortIterator extends SplHeap
{
    public function __construct(Iterator $iterator)
    {
        foreach ($iterator as $item) {
            $this->insert($item);
        }
    }

    public function compare($b,$a) : int
    {
        $a = new SplFileInfo($a);
        $b = new SplFileInfo($b);
        if($a->getPath() == $b->getPath()) return strcmp($a->getRealpath(), $b->getRealpath());
        if(str_contains($a->getPath(), $b->getPath())) return 1;
        if(str_contains($b->getPath(), $a->getPath())) return -1;
        return strcmp($a->getRealpath(), $b->getRealpath());
    }
}
