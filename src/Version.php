<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;

/**
 * Scripts
 */
class Version
{
	/**
	 * Buscar elemento en archivo y retornar listado de resultados
	 *
	 * @param string $filename
	 * @param string $search
	 * @return string
	 */
	private static function search_data($file, $search): string
	{
		if (file_exists($file)) {
			// Leer el archivo style.css
			$file_data = file_get_contents($file);
			// Buscar el elemento
			if (preg_match_all('/' . $search . '/', $file_data, $result)) {
				// Si hubo respuesta
				if (isset($result[1][0])) {
					return $result[1][0];
				}
			}
		}

		// Si no hubo respuesta retornar vacio
		return '';
	}

	/**
	 * Actualizar las versiones
	 */
	public static function update(): array
	{
		$version = false;
		$dir     = getcwd();
		$type    = basename(dirname($dir));
		MerakEnv::isMerak();

		$merak = json_decode(file_get_contents($dir . '/merak.json'));

		// Buscar el elemento que tiene la versión
		if ($type == 'themes') {
			// Leer el archivo style.css
			$file_data = file_get_contents($dir . '/style.css');

			if (preg_match_all('/Version: ([0-9]+\.[0-9]+\.[0-9]+)/i', $file_data, $result)) {
				if (isset($result[1][0])) {
					$version = $result[1][0];

					// Modificar README.md
					$file   = $dir . '/README.md';
					$search = self::search_data($file, 'Versión\*\* ([0-9]+\.[0-9]+\.[0-9]+)');
					if (!empty($search)) {
						file_put_contents($file, str_replace("Versión** {$search}", "Versión** {$version}", file_get_contents($file)));
					}

					// Modificar package.json
					$file   = $dir . '/package.json';
					$search = self::search_data($file, '"version": "([0-9]+\.[0-9]+\.[0-9]+)"');
					if (!empty($search)) {
						file_put_contents($file, str_replace('"version": "' . $search . '"', '"version": "' . $version . '"', file_get_contents($file)));
					}

					// Modificar composer.json
					$file   = $dir . '/composer.json';
					$search = self::search_data($file, '"version": "([0-9]+\.[0-9]+\.[0-9]+)"');
					if (!empty($search)) {
						file_put_contents($file, str_replace('"version": "' . $search . '"', '"version": "' . $version . '"', file_get_contents($file)));
					}
				}
			}
		} elseif ($type == 'plugins') {
			$file_data = file_get_contents($dir . '/' . $merak->filename . '.php');
			// Buscar el elemento que tiene la versión
			if (preg_match_all('/Version: ([0-9]+\.[0-9]+\.[0-9]+)/i', $file_data, $result)) {
				if (isset($result[1][0])) {
					$version = $result[1][0];

					// Modificar README.md
					$file   = $dir . '/README.md';
					$search = self::search_data($file, 'Versión\*\* ([0-9]+\.[0-9]+\.[0-9]+)');
					if (!empty($search)) {
						file_put_contents($file, str_replace("Versión** {$search}", "Versión** {$version}", file_get_contents($file)));
					}

					// Modificar package.json
					$file   = $dir . '/package.json';
					$search = self::search_data($file, '"version": "([0-9]+\.[0-9]+\.[0-9]+)"');
					if (!empty($search)) {
						file_put_contents($file, str_replace('"version": "' . $search . '"', '"version": "' . $version . '"', file_get_contents($file)));
					}

					// Modificar composer.json
					$file   = $dir . '/composer.json';
					$search = self::search_data($file, '"version": "([0-9]+\.[0-9]+\.[0-9]+)"');
					if (!empty($search)) {
						file_put_contents($file, str_replace('"version": "' . $search . '"', '"version": "' . $version . '"', file_get_contents($file)));
					}
				}
			}
		}

		return [$type, $version];
	}
}
