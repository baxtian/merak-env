<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;

/**
 * Scripts
 */
class Libs
{
	/**
	 * Actualizar las versiones
	 */
	static public function update() : bool
	{
		$answer = false;
		$dir = getcwd();
		MerakEnv::isMerak();

		/***** BLOCK.JS *****/

		// Buscar los json en el directorio de librerías
		$files = glob('assets/libs/*/lib.json');
		$data  = [];

		foreach ($files as $file) {
			// Extraer la información
			$data = array_merge($data, json_decode(file_get_contents($file), true));
		}

		// Guardar archivo o eliminar
		$filename = $dir . '/assets/libs/lib.json';
		if (count($data) > 0) {
			$text = json_encode($data);
			$text = str_replace("\/", '/', $text);
			file_put_contents($filename, $text);
			$answer = true;
		} elseif (file_exists($filename)) {
			unlink($filename);
			$answer = false;
		}

		return $answer;
	}
}
