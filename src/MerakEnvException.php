<?php

namespace MerakEnv;

use Exception;
use Throwable;

/**
 * Define a custom exception class
 */
class MerakEnvException extends Exception
{
	public const NO_MERAK_ENVIRONMENT = 100;
	public const NO_SVN_CONFIG        = 200;
	public const NO_ZIP               = 201;
	public const NO_SVN_DIRECTORY     = 202;
	public const NO_VERSION           = 203;

	// Redefine the exception so message isn't optional
	public function __construct($message, $code = 0, Throwable $previous = null)
	{
		// some code

		// make sure everything is assigned properly
		parent::__construct($message, $code, $previous);
	}

	// custom string representation of object
	public function __toString()
	{
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}
}
