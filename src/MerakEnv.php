<?php

namespace MerakEnv;

use MerakEnv\MerakEnvException;

/**
 * Scripts
 */
class MerakEnv
{
	public static function isMerak(): bool
	{
		if (!file_exists(getcwd() . '/merak.json') || !file_exists(getcwd() . '/composer.json')) {
			throw new MerakEnvException("This isn't a Merak environment.", MerakEnvException::NO_MERAK_ENVIRONMENT);
		}

		return true;
	}

	public static function getType(): string
	{
		self::isMerak();

		$package_file = getcwd() . DIRECTORY_SEPARATOR . 'merak.json';
		$package_json = json_decode(file_get_contents($package_file), true);
		return (isset($package_json['type'])) ? $package_json['type'] : "none";
	}

	public static function deleteLangJson() {
		$path  = 'languages';
		$files = glob("$path/*.json");
		foreach ($files as $file) {
			unlink($file);
		}
	}

	public static function deleteWpCli() {
		$files = glob("wp-cli.phar");
		foreach ($files as $file) {
			unlink($file);
		}
	}
}
