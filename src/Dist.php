<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;

/**
 * Scripts
 */
class Dist
{
	/**
	 * Crea el archivo de distribución usando composer
	 */
	public static function archive(): bool
	{
		MerakEnv::isMerak();

		// Buscar datos merak.json
		$json_file = getcwd() . '/merak.json';
		$json      = json_decode(file_get_contents($json_file), true);

		$filename = $json['filename'];

		// Comprimir
		exec("composer archive --format=zip --file='{$filename}'");

		return true;
	}

	/**
	 * Elimina el archivo de distribución
	 *
	 * @return boolean
	 */
	public static function clear(): bool
	{
		MerakEnv::isMerak();

		// Buscar datos merak.json
		$json_file = getcwd() . '/merak.json';
		$json      = json_decode(file_get_contents($json_file), true);

		$file = getcwd() . '/' . $json['filename'] . '.zip';
		if (file_exists($file)) {
			unlink($file);
		}

		return true;
	}
}
