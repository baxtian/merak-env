<?php

namespace MerakEnv;

use MerakEnv\MerakEnv;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;
use MerakEnv\SortIterator;

/**
 * Scripts
 */
class Src
{
	/**
	 * Actualizar las versiones
	 */
	public static function update(): bool
	{
		$answer = false;
		$dir    = getcwd();
		MerakEnv::isMerak();

		/***** SRC *****/

		// Buscar los .php del src
		$directory = new RecursiveDirectoryIterator($dir . DIRECTORY_SEPARATOR . 'src');
		$iterator  = new RecursiveIteratorIterator($directory);
		$sit = new SortIterator($iterator);
		$files     = new RegexIterator($sit, '/^.+\.php$/i', RegexIterator::GET_MATCH);
		$fileList  = [];

		foreach ($files as $file) {
			$content = file_get_contents($file[0]);
			// Buscar si el archivo tiene "use \Baxtian\Singleton;"
			if (
				strpos($content, 'use \Baxtian\Singleton;') !== false ||
				strpos($content, 'use \Baxtian\SingletonTrait;') !== false
			) {
				$fileList[] = $file[0];
			}
		}

		$merak = json_decode(file_get_contents(getcwd() . '/merak.json'), true);

		// Inicializar texto del archivo y el contador
		$text  = '';
		$items = 0;

		foreach ($fileList as $item) {
			// Retirar el directorio que se usó de referencia y la extensión
			$item = str_replace([$dir . DIRECTORY_SEPARATOR . 'src', '.php'], '', $item);
			// Extraer las partes usando el separador de directorios
			$item = explode(DIRECTORY_SEPARATOR, $item);
			// Retirar el primero de los elementos (estará vacío)
			array_shift($item);

			// Crear el espacio de nombres para este archivo
			$element = $merak['namespace'] . '\\' . implode('\\', $item);

			// Si es el Init
			if (
				(count($item) == 1 && $item[0] == 'Init')
			) {
				// Poner al inicio
				$text = "$element::get_instance();\n" . $text;
			} else {
				// Poner al final
				$text = $text . "$element::get_instance();\n";
			}

			$items++;
		}

		// Agregar apertura php
		$text = "<?php\n\n" . $text;

		// Guardar archivo
		$filename = $dir . '/src/Instances.php';

		if ($items > 0) {
			file_put_contents($filename, $text);
			$answer = true;
		} elseif (file_exists($filename)) {
			unlink($filename);
			$answer = false;
		}

		return $answer;
	}
}
